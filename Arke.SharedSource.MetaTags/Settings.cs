﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Links;

namespace Arke.SharedSource.MetaTags
{

	public enum MetaTagType
	{
		name,
		property
	}

	public class Settings
	{

		public const string PIPELINE_NAME = "Arke.MetaTags.InjectMetaTags";


		public static string ContextObjectName
		{
			get
			{
				return Sitecore.Configuration.Settings.GetSetting("Arke.MetaTags.ContextObjectName");
			}
		}

		public static string GlobalTagsFolder
		{
			get
			{
				return Sitecore.Configuration.Settings.GetSetting("Arke.MetaTags.GlobalTagsFolder");
			}
		}

		public const int MAX_TAG_PROFILE_LENGTH = 100;
		public static int MaxTagProfileLength
		{
			get
			{
				return Sitecore.Configuration.Settings.GetIntSetting("Arke.MetaTags.MaxTagProfileLength", MAX_TAG_PROFILE_LENGTH);
			}
		}


		public static MetaTagType GetTypeFromString(string tagType)
		{
			switch (tagType.ToLower())
			{
				case "name":
					return MetaTagType.name;
					break;
				case "property":
					return MetaTagType.property;
					break;
				default:
					return MetaTagType.name;
					break;
			}
		}


		public static string GetAuthorativeUrl()
		{
			Sitecore.Links.UrlOptions opt = Sitecore.Links.LinkManager.GetDefaultUrlOptions();
			opt.AlwaysIncludeServerUrl = true;
			return LinkManager.GetItemUrl(Sitecore.Context.Item, opt);
		}



	}
}
