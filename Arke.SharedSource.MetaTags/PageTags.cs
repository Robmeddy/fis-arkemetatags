﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Arke.SharedSource.MetaTags.Tags;

namespace Arke.SharedSource.MetaTags
{
	public class PageTags
	{
		public static List<BaseTag> AllTags
		{
			get
			{
				return (List<BaseTag>)Sitecore.Context.Items[Settings.ContextObjectName];
			}
			set
			{
				Sitecore.Context.Items[Settings.ContextObjectName] = value;
			}
		}

		public static void AddTag(MetaTagType TagType, string Key, string Value)
		{
			AddTag(new StaticTag(TagType, Key, Value));
		}

		public static void AddTag(BaseTag Tag)
		{
			AllTags.Add(Tag);
		}

	}
}
