﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Pipelines;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Sitecore.Links;
using Arke.SharedSource.MetaTags.Tags;

namespace Arke.SharedSource.MetaTags
{
	public class InjectMetaTagsPipelineArgs : PipelineArgs
	{
		public List<BaseTag> MetaTags;

		public InjectMetaTagsPipelineArgs()
		{
			this.MetaTags = new List<BaseTag>();
		}

	}
}