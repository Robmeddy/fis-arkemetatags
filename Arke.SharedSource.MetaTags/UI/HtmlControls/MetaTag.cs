﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Arke.SharedSource.MetaTags.UI.HtmlControls
{
	public class MetaTag : System.Web.UI.HtmlControls.HtmlControl
	{

		public MetaTagType TagType { get; set; }
		public string Key { get; set; }
		public string Value { get; set; }

		public MetaTag()
			: base()
		{
			TagType = MetaTagType.name;
		}

		public MetaTag(MetaTagType TagType, string Key, string Value)
			: base()
		{
			this.TagType = TagType;
			this.Key = Key;
			this.Value = Value;
		}


		protected override void Render(HtmlTextWriter writer)
		{
			Attributes.Add(TagType.ToString(), Key);
			Attributes.Add("content", Value);
			writer.Write(HtmlTextWriter.TagLeftChar + "meta");
			Attributes.Render(writer);
			writer.Write(HtmlTextWriter.SelfClosingTagEnd);
		}

		public override ControlCollection Controls
		{
			get { throw new NotImplementedException(); }
		}



	}
}
