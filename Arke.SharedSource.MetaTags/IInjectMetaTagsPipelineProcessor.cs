﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arke.SharedSource.MetaTags.Pipelines
{
	public interface IInjectMetaTagsPipelineProcessor
	{
		void Process(InjectMetaTagsPipelineArgs args);
	}
}
