﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Arke.SharedSource.MetaTags.UI.HtmlControls;
using Sitecore.Diagnostics;
using Sitecore.Data.Items;
using Sitecore.Data;

namespace Arke.SharedSource.MetaTags.Tags
{
	public class MethodTag : BaseTag
	{
		private string typeSignature = "[unknown]";
		private string methodName = "[unknown]";

		public MethodTag(string TypeSignature, string MethodName, string Key) 
			: this(TypeSignature, MethodName, Key, MetaTagType.name)
		{}

		public MethodTag(string TypeSignature, string MethodName, string Key, MetaTagType TagType)
		{
			Assert.ArgumentNotNullOrEmpty(TypeSignature, "TypeSignature");
			Assert.ArgumentNotNullOrEmpty(MethodName, "MethodName");
			Assert.ArgumentNotNullOrEmpty(Key, "TagName");

			this.typeSignature = TypeSignature;
			this.methodName = MethodName;

			string metaValue = ReflectionUtil.GetStringFromMethod(TypeSignature, MethodName);
			Assert.IsNotNullOrEmpty(metaValue, "Value returned from method " + TypeSignature + " " + MethodName);
			
			this.TagType = TagType;
			this.Key = Key;
			this.Value = metaValue;
		}

		public MethodTag(Item item)
		{
			Assert.IsNotNull(item, "Item");
			Assert.IsTrue(item.TemplateID.Equals(IDs.Templates.MethodMetaTag), "Item must be a MethodTag");

			this.typeSignature = item["TypeSignature"];
			Assert.IsNotNullOrEmpty(typeSignature, "TypeSignature");

			this.methodName = item["MethodName"];
			Assert.IsNotNullOrEmpty(methodName, "MethodName");

			string tType = item["Type"];
			Assert.IsNotNullOrEmpty(tType, "Tag Type");

			Key = item["Key"];
			Value = ReflectionUtil.GetStringFromMethod(item["TypeSignature"], item["MethodName"]);
			TagType = Settings.GetTypeFromString(item["Type"]);
		}



		public override string GetName()
		{
			string method = ReflectionUtil.GetMethodName(this.typeSignature, this.methodName, "unknown method");
			return base.GetName() + " [" + method + "]";
		}

	}
}