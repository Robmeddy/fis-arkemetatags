﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Arke.SharedSource.MetaTags.UI.HtmlControls;
using Sitecore.Diagnostics;
using Sitecore.Data.Items;
using Sitecore.Data;
using System.Web;

namespace Arke.SharedSource.MetaTags.Tags
{
    public class StaticTag : BaseTag
    {

        public StaticTag(MetaTagType TagType, String Key, string Value)
        {
            this.TagType = TagType;
            this.Key = Key;
            this.Value = Value;
        }
        
        public StaticTag(Item item)
        {
            Sitecore.Diagnostics.Log.Info("MNA Arke: "+item.Name, this);
            Assert.IsNotNull(item, "Item");
            Assert.IsTrue(item.TemplateID.Equals(IDs.Templates.StaticMetaTag), "Item must be a StaticMetaTag");

            //string NewsArticleTemplateGUID = "{EDB6C81F-D695-47D4-B7F4-BBB3C82AEE51}";
            //string EventArticleTemplateGUID = "{09D87106-9CCC-4A9C-A700-41C9C8A691AA}";
            string PageUrl = string.Empty;

            Database d = Sitecore.Context.Database;

            Sitecore.Links.UrlOptions dOpts = new Sitecore.Links.UrlOptions();
            Sitecore.Links.UrlOptions uo = new Sitecore.Links.UrlOptions();
            uo.AlwaysIncludeServerUrl = true;
            uo.LanguageEmbedding = Sitecore.Links.LanguageEmbedding.Never;
            dOpts = Sitecore.Links.LinkManager.GetDefaultUrlOptions();
            PageUrl = Sitecore.Links.LinkManager.GetItemUrl(Sitecore.Context.Item, uo);


            Sitecore.Resources.Media.MediaUrlOptions opt = new Sitecore.Resources.Media.MediaUrlOptions();
            opt.AlwaysIncludeServerUrl = true;

            string[] splits = PageUrl.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            int DescLength = 154;
            Key = item["Key"];
            Item i = null;

            if (item.Name.Contains("__MO_MNA_"))
            {
                #region TemplateField GUIDS
                /*  These should probably be guids.
                 *  event name          {175631B4-BAE5-43C8-9F9E-F4DD6CC05F0F}
                    event date from     {61853C23-A2E8-4C68-A4E3-3E3CC8F44763}
                    event date to       {95ECE38C-9AC4-45BA-AA6F-49BBA57A0392}
                    Event Location      {FF1E4AB7-D3BC-4264-AC22-A0BD0467ED6B}
                    Event Description   {B582179B-74F4-42AB-8161-C0FD85A94EDA}
                    event image         {4988E4BF-3AB7-4484-88BB-32A164E243D3}

                    storydate           {ED25D652-DC02-4E3E-BC7B-3583047650F5}
                    storyLocation       {9B6D3BBE-599D-4240-AEC5-0CEE74E1B814}
                    storyByLine         {548D41D2-9B5A-4C13-913A-59CEA2BEBA09}
                    storyHeadline       {379FD1D4-3FD2-47B6-8F70-9E50925B2752}
                    storyImage          {172AE69A-8A6F-4CFC-ACDD-7075C6F4E876}
                    storyVideo          {13E22BCF-8196-40FA-AF89-8334E687AC05}
                    storyText           {AD719E21-893E-4C89-9EE7-27A4DE7071C9}
                    */
                #endregion

                TagType = Settings.GetTypeFromString(item["Type"]);
                switch (item.Name)
                {
                    case "__MO_MNA_MetaDescription":
                    case "__MO_MNA_GoogleDescription":
                    case "__MO_MNA_OpenGraphDescription":
                    case "__MO_MNA_TwitterDescription":
                        if (Regex.IsMatch(Sitecore.Context.Item.Fields["{AD719E21-893E-4C89-9EE7-27A4DE7071C9}"].Value, "<.*?>", RegexOptions.IgnoreCase))
                        {
                            Value = Regex.Replace(Sitecore.Context.Item.Fields["{AD719E21-893E-4C89-9EE7-27A4DE7071C9}"].Value, "<.*?>", string.Empty);
                        }
                        else
                        {
                            Value = Sitecore.Context.Item.Fields["{AD719E21-893E-4C89-9EE7-27A4DE7071C9}"].Value;
                        }

                        if (Value.Length > DescLength)
                        {
                            Value = Value.Substring(0, DescLength);
                        }
                        break;
                    case "__MO_MNA_OpenGraphSiteName":
                    case "__MO_MNA_TwitterSite":
                    case "__MO_MNA_GoogleName":
                        string HostName = HttpContext.Current.Request.Url.Host;
                        Value = HostName;
                        break;
                    case "__MO_MNA_GoogleImage":
                    case "__MO_MNA_OpenGraphImage":
                    case "__MO_MNA_TwitterImage":
                        Sitecore.Data.Fields.ImageField imageField = null;
                        MediaItem mi = null;
                        if (Sitecore.Context.Item.Fields["StoryImage"] != null)
                        {
                            imageField = Sitecore.Context.Item.Fields["StoryImage"];
                            if (imageField.MediaItem != null) {
                                mi = new MediaItem(imageField.MediaItem);
                            }
                        }
                        if (mi != null)
                        {
                            Value = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mi, opt);
                        }
                        else
                        {
                            Value = "OPENROAD";
                        }
                        break;
                    case "__MO_MNA_OpenGraphTitle":
                    case "__MO_MNA_TwitterTitle":
                        Value = Sitecore.Context.Item.Name;
                        break;
                    case "__MO_MNA_OpenGraphType":
                        Value = "website";
                        break;
                    case "__MO_MNA_OpenGraphURL":
                        Value = PageUrl;
                        break;
                    case "__MO_MNA_TwitterCard":
                        Value = "Summary";
                        break;
                    case "__MO_MNA_TwitterCreator":
                        Value = string.Empty;
                        string temp = Sitecore.Context.Item.Fields["__Updated by"].ToString();
                        string[] NameSplitter = temp.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                        if (NameSplitter.Length > 1)
                        {
                            Value = NameSplitter[1];
                        }
                        break;
                    case "__MO_MNA_ArticlePublishedTime":
                        Value = item.Statistics.Updated.ToString();
                        break;
                    case "__MO_MNA_ArticleModifiedTime":

                        Value = item.Statistics.Updated.ToString();
                        break;
                    case "__MO_MNA_ArticleSection":
                        Value = "ArticleSection?";
                        break;
                    case "__MO_MNA_ArticleTag":
                        Sitecore.Data.Fields.MultilistField mf = Sitecore.Context.Item.Fields["__Semantics"];
                        if (mf.Count > 0)
                        {
                            StringBuilder sb = new StringBuilder();
                            for (int y = 0; y < mf.Count; y++)
                            {
                                Item DataItem = d.Items.GetItem(mf[y]);
                                sb.Append(DataItem.Name + ",");
                            }
                            Value = sb.ToString(0, sb.Length - 1);
                        }
                        break;
                    case "__MO_MNA_FacebookAdmins":
                        Value = "Facebook Numeric ID";
                        break;
                    case "__MO_MNA_TwitterAppCountry":
                        Value = "USA";
                        break;
                    case "__MO_MNA_TwitterAppNameIPhone":
                    case "__MO_MNA_TwitterAppIdIPhone":

                    case "__MO_MNA_twitterAppUrlIPhone":

                    case "__MO_MNA_TwitterAppNameIPad":

                    case "__MO_MNA_TwitterAppIdIPad":

                    case "__MO_MNA_TwitterAppUrlIPad":
                        Value = item.Fields["Value"].Value;
                        break;
                };
            }
            else
            {
                Key = item["Key"];
                Value = item["Value"];
                TagType = Settings.GetTypeFromString(item["Type"]);
            }
        }
    }
}
/*
 *
 *
 * case "__MO_MNA_TwitterDescription":
  D:\code\ModOp\Arke\Arke.SharedSource.MetaTags\Tags\StaticTag.cs(97):                    case "__MO_MNA_TwitterSite":
  D:\code\ModOp\Arke\Arke.SharedSource.MetaTags\Tags\StaticTag.cs(104):                    case "__MO_MNA_TwitterImage":
  D:\code\ModOp\Arke\Arke.SharedSource.MetaTags\Tags\StaticTag.cs(124):                    case "__MO_MNA_TwitterTitle":
  D:\code\ModOp\Arke\Arke.SharedSource.MetaTags\Tags\StaticTag.cs(134):                    case "__MO_MNA_TwitterCard":
  D:\code\ModOp\Arke\Arke.SharedSource.MetaTags\Tags\StaticTag.cs(137):                    case "__MO_MNA_TwitterCreator": 

<meta property="og:image" content=“[INSERT ARTICLE IMAGE OR FIS LOGO]“> 
<meta property="og:type" content=“article”>
<meta property="og:site_name" content=“fis-global“>
<meta property="og:title" content=“FIS Global | [INSERT ARTICLE TITLE]“>
<meta property="og:url" content=“[INSERT SHARE URL]“>
<meta property="og:description" content=“[INSERT ARTICLE DESCRIPTION OR FIS GLOBAL SITE DESCRIPTION]“>

## <meta name="twitter:card" content=“summary”>
##<meta name="twitter:title" content=“FIS Global | [INSERT TITLE HERE]“>
<meta name="twitter:player" content=""> <<< REMOVE THIS UNTIL WE GET TO VIDEO CONTENT IN A LATER RELEASE 
##<meta name="twitter:site" content=“FIS Global“>
<meta name="twitter:description" content=“[INSERT ARTICLE DESCRIPTION OR FIS GLOBAL SITE DESCRIPTION]“>
##<meta name="twitter:image" content="[INSERT ARTICLE IMAGE OR FIS LOGO]">
— new tags — 

<meta name="twitter:app:country” content=“USA”>
__MO_MNA_TwitterAppCountry
<meta name="twitter:app:name:iphone" content=“FIS Global News“>
__MO_MNA_TwitterAppCountry__MO_MNA_TwitterAppNameIPhone
<meta name="twitter:app:id:iphone" content=“[INSERT APP ID ONCE AVAILABLE]”>
__MO_MNA_TwitterAppCountry__MO_MNA_TwitterAppNameIPhone__MO_MNA_TwitterAppIdIPhone
<meta name="twitter:app:url:iphone" content="[INSERT APP CUSTOM URL SCHEME URI HERE]">
__MO_MNA_TwitterAppCountry__MO_MNA_TwitterAppNameIPhone__MO_MNA_TwitterAppIdIPhone__MO_MNA_twitterAppUrlIPhone
<meta name="twitter:app:name:ipad" content=“FIS Global News“>
__MO_MNA_TwitterAppCountry__MO_MNA_TwitterAppNameIPhone__MO_MNA_TwitterAppIdIPhone__MO_MNA_twitterAppUrlIPhone__MO_MNA_TwitterAppNameIPad
<meta name="twitter:app:id:ipad" content="[INSERT APP ID ONCE AVAILABLE]">
__MO_MNA_TwitterAppCountry__MO_MNA_TwitterAppNameIPhone__MO_MNA_TwitterAppIdIPhone__MO_MNA_twitterAppUrlIPhone__MO_MNA_TwitterAppNameIPad__MO_MNA_TwitterAppIdIPad
<meta name="twitter:app:url:ipad" content=“[INSERT APP CUSTOM URL SCHEME URI HERE]“>
__MO_MNA_TwitterAppCountry__MO_MNA_TwitterAppNameIPhone__MO_MNA_TwitterAppIdIPhone__MO_MNA_twitterAppUrlIPhone__MO_MNA_TwitterAppNameIPad__MO_MNA_TwitterAppIdIPad__MO_MNA_TwitterAppUrlIPad

    __MO_MNA_Twitter:

    <meta property="fb:admins" content="133955986688773"> <<< Something FIS uses on their site currently… may not need to include this.
 */
