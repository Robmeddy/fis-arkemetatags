﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Diagnostics;
using Arke.SharedSource.MetaTags.UI.HtmlControls;

namespace Arke.SharedSource.MetaTags.Tags
{
	public abstract class BaseTag
	{
		protected MetaTagType TagType { get; set; }
		protected string Key { get; set; }
		protected string Value { get; set; }

		public BaseTag()
		{
			this.TagType = MetaTagType.name;
		}

		public virtual System.Web.UI.Control GetControl()
		{
			Assert.IsNotNullOrEmpty(Key, "Key");
			Assert.IsNotNullOrEmpty(Value, "Value");
			return new MetaTag(TagType, Key, Value);
		}

		public virtual string GetName()
		{
			string name = Key + ": " + Value;
			if (name.Length > Settings.MaxTagProfileLength) 
			{
				name = name.Substring(0, Settings.MaxTagProfileLength - 3) + "...";
			}
			return name;
		}

	}
}
