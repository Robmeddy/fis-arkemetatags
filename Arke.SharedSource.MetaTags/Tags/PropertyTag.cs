﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Arke.SharedSource.MetaTags.UI.HtmlControls;
using Sitecore.Diagnostics;
using Sitecore.Data.Items;
using Sitecore.Data;
using System.Reflection;

namespace Arke.SharedSource.MetaTags.Tags
{
	public class PropertyTag : BaseTag
	{
		private string typeSignature = "[unknown]";
		private string propertyName = "[unknown]";

		public PropertyTag(string TypeSignature, string PropertyName, string Key)
			: this(TypeSignature, PropertyName, Key, MetaTagType.name)
		{}

		public PropertyTag(string TypeSignature, string PropertyName, string Key, MetaTagType TagType)
		{
			Assert.ArgumentNotNullOrEmpty(TypeSignature, "TypeSignature");
			Assert.ArgumentNotNullOrEmpty(PropertyName, "PropertyName");
			Assert.ArgumentNotNullOrEmpty(Key, "TagName");

			this.propertyName = PropertyName;
			this.typeSignature = TypeSignature;

			string metaValue = ReflectionUtil.GetStringFromPropery(TypeSignature, PropertyName);
			Assert.IsNotNullOrEmpty(metaValue, "Value returned from property " + TypeSignature + " " + PropertyName);
			
			this.TagType = TagType;
			this.Key = Key;
			this.Value = metaValue;

		}

		public PropertyTag(Item item)
		{
			Assert.IsNotNull(item, "Item");
			Assert.IsTrue(item.TemplateID.Equals(IDs.Templates.PropertyMetaTag), "Item must be a PropertyTag");

			this.propertyName = item["PropertyName"];
			Assert.IsNotNullOrEmpty(this.propertyName, "PropertyName");

			this.typeSignature = item["TypeSignature"];
			Assert.IsNotNullOrEmpty(this.typeSignature, "TypeSignature");

			string tType = item["Type"];
			Assert.IsNotNullOrEmpty(tType, "Tag Type");

			this.TagType = Settings.GetTypeFromString(tType);
			this.Key = item["Key"];
			this.Value = ReflectionUtil.GetStringFromPropery(this.typeSignature, this.propertyName);
		}



		public override string GetName()
		{
			string property = ReflectionUtil.GetPropertyName(this.typeSignature, this.propertyName, "unknown");
			return base.GetName() + " [" + property + "]";
		}

	}
}