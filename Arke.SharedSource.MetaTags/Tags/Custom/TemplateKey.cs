﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arke.SharedSource.MetaTags.Tags.Custom
{
	public class TemplateKey : BaseTag
	{
		public TemplateKey()
		{
			TagType = MetaTagType.name;
			Key = "TemplateKey";
			Value = Sitecore.Context.Item.Template.Key;
		}

	}
}
