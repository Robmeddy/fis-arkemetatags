﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arke.SharedSource.MetaTags.Tags.Custom
{
	public class MachineName : BaseTag
	{
		public MachineName()
		{
			TagType = MetaTagType.name;
			Key = "MachineName";
			Value = System.Environment.MachineName;
		}

	}
}
