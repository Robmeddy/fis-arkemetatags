﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arke.SharedSource.MetaTags.Tags.Custom
{
	public class CanonicalUrl : BaseTag
	{
		public CanonicalUrl()
		{
			TagType = MetaTagType.name;
			Key = "canonical";
			Value = Settings.GetAuthorativeUrl();
		}

	}
}
