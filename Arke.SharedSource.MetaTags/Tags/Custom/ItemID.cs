﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arke.SharedSource.MetaTags.Tags.Custom
{
	public class ItemID : BaseTag
	{
		public ItemID()
		{
			TagType = MetaTagType.name;
			Key = "ItemID";
			Value = Sitecore.Context.Item.ID.ToString();
		}

	}
}
