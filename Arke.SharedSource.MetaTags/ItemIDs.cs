﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Data;

namespace Arke.SharedSource.MetaTags
{
	public class IDs
	{
		public class Templates
		{
			public static ID StaticMetaTag = new ID("{A84190EB-A8CF-46A8-85B2-960DBE12099C}");
			public static ID CustomMetaTag = new ID("{2FCB4595-C3C1-4469-B10D-CFD68799ADED}");
			public static ID MethodMetaTag = new ID("{0A91E1DB-1F19-4EF1-B369-FF8A411B5B85}");
			public static ID PropertyMetaTag = new ID("{E8E0CB48-05B7-4423-9C2A-0C6A4B518CB7}");
			public static ID ItemMetaTagsTemplate = new ID("{DFAF6371-E156-4634-A3EC-6DF507ECE3F2}");
		}

		public class Fields
		{
			public static ID ItemMetaTagList = new ID("{89763FF0-9363-4C88-BB19-6080BC5F5FE0}");
		}

	}
}
