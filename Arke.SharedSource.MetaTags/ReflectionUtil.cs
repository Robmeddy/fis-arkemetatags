﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Diagnostics;
using System.Reflection;
using Sitecore.Data.Items;

namespace Arke.SharedSource.MetaTags
{
	public class ReflectionUtil
	{

		public static string GetStringFromPropery(string TypeSignature, string PropertyName)
		{
			try
			{
				Type t = Sitecore.Reflection.ReflectionUtil.GetTypeInfo(TypeSignature);
				Assert.IsNotNull(t, "Type '" + TypeSignature + "' could not be acessed");

				PropertyInfo pInfo = t.GetProperty(PropertyName);
				Assert.IsNotNull(pInfo, "Property '" + PropertyName + "' not accesible in '" + TypeSignature + "'");
				Assert.IsTrue(pInfo.CanRead, "PropertyName '" + PropertyName + "' cannot be read.");

				return pInfo.GetValue(null, null).ToString();
			}
			catch
			{
				return null;
			}
		}

		public static string GetStringFromMethod(string TypeSignature, string MethodName)
		{

			try
			{
				return Sitecore.Reflection.ReflectionUtil.CallStaticMethod(TypeSignature, MethodName, new object[] { }).ToString();
			}
			catch
			{
				return null;
			}
		}


		public static Tags.BaseTag GetCustomTag(Item TagItem)
		{
			Assert.IsNotNullOrEmpty(TagItem["TypeSignature"], "TypeSignature");
			return GetCustomTag(TagItem["TypeSignature"]);
		}

		public static Tags.BaseTag GetCustomTag(string TypeSignature)
		{
			Type type = Sitecore.Reflection.ReflectionUtil.GetTypeInfo(TypeSignature);
			Assert.IsNotNull(type, "Type '" + TypeSignature + "' not found");
			object obj = Sitecore.Reflection.ReflectionUtil.CreateObject(type, new object[] { });
			return (Tags.BaseTag)obj;
		}

		public class TypeSignature
		{
			public string Class { get; set; }
			public string Assembly { get; set; }

			public TypeSignature(string Signature)
			{
				string[] tSig = Sitecore.StringUtil.Split(Signature, ',', true);
				Assert.IsNotNull(tSig, "Signature malformed");
				Assert.IsTrue(tSig.Length == 2, "Signature malformed");
				Class = tSig[0];
				Assembly = tSig[1];
			}
		}

		public static string GetMethodName(string TypeSignature, string MethodName, string DefaultValue)
		{
			try
			{
				return new ReflectionUtil.TypeSignature(TypeSignature).Class + "." + MethodName + "()";
			}
			catch
			{
				return String.IsNullOrEmpty(DefaultValue) ? "[unknown]" : DefaultValue;
			}
		}

		public static string GetPropertyName(string TypeSignature, string PropertyName, string DefaultValue)
		{
			try
			{
				return new ReflectionUtil.TypeSignature(TypeSignature).Class + "." + PropertyName;
			}
			catch
			{
				return String.IsNullOrEmpty(DefaultValue) ? "[unknown]" : DefaultValue;
			}
		}

	}
}
