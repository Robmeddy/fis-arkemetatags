﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Diagnostics;
using Sitecore.Pipelines;
using Sitecore.Pipelines.InsertRenderings;


namespace Arke.SharedSource.MetaTags.Pipelines.InsertRenderings
{
	public class InjectMetaTags
	{

		#region Process method -------------------------------------------------------------------------------------------------

		public void Process(InsertRenderingsArgs args)
		{
			Assert.ArgumentNotNull(args, "args");
            var ar = args;
			// Don't wire this up when in the sitecore shell
			if (Sitecore.Context.Site.Name.Equals("shell", StringComparison.InvariantCultureIgnoreCase))
			{
				Tracer.Warning("Meta tags not emitted in the shell site.");
				return;
			}

			// We'll wire up a handler for after prerender is complete, so any meta tags added
			// by controls (via Arke.SharedSource.MetaTags.PageTags) will be available.
			Sitecore.Context.Page.Page.PreRenderComplete += new EventHandler(RunPipeline);

		}

		#endregion

		#region Lifecycle methods -------------------------------------------------------------------------------------------------

		void RunPipeline(object sender, EventArgs e)
		{
			Profiler.StartOperation("Injecting meta tags.");
			try
			{
				InjectMetaTagsPipelineArgs args = new InjectMetaTagsPipelineArgs();
				Sitecore.Pipelines.CorePipeline.Run(Settings.PIPELINE_NAME, args);
			}
			catch (Exception ex)
			{
				Sitecore.Diagnostics.Tracer.Error("InjectMetaTags failed", ex);
				Sitecore.Diagnostics.Log.Error("InjectMetaTags failed", ex, "InjectMetaTags");
			}
			Profiler.EndOperation();
		}

		#endregion


	}
}

