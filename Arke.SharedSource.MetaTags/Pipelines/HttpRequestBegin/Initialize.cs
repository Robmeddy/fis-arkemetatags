﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Diagnostics;
using Sitecore;
using Sitecore.Pipelines;

namespace Arke.SharedSource.MetaTags.Pipelines.HttpRequestBegin
{
	public class Initialize : Sitecore.Pipelines.HttpRequest.HttpRequestProcessor
	{
		public override void Process(Sitecore.Pipelines.HttpRequest.HttpRequestArgs args)
		{
			Assert.ArgumentNotNull(args, "args");
			if (!Context.Items.Contains(Settings.ContextObjectName))
			{
				Context.Items.Add(Settings.ContextObjectName, new List<Arke.SharedSource.MetaTags.Tags.BaseTag>());
			}
		}
	}
}
