﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using Sitecore.Diagnostics;
using Arke.SharedSource.MetaTags.Tags;

namespace Arke.SharedSource.MetaTags.Pipelines.InjectMetaTags
{
	public class FlushMetaTags : IInjectMetaTagsPipelineProcessor
	{

		public void Process(InjectMetaTagsPipelineArgs args)
		{
			args.MetaTags.AddRange(PageTags.AllTags);

			Profiler.StartOperation("Flushing meta tag controls.");
			if (args.MetaTags.Count > 0)
			{
				foreach (BaseTag tag in args.MetaTags)
				{
					Profiler.StartOperation(tag.GetName());
					try
					{
						Sitecore.Context.Page.Page.Header.Controls.Add(tag.GetControl());
					}
					catch (Exception ex)
					{
						Tracer.Error("Failed to inject tag '" + tag.GetName() + "'.", ex);
					}
					finally
					{
						Profiler.EndOperation();
					}
				}
			}

			Profiler.EndOperation();
		}

	}
}	