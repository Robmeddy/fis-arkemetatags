﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Diagnostics;
using System.Runtime.Remoting;
using System.Reflection;
using Arke.SharedSource.MetaTags.Tags;

namespace Arke.SharedSource.MetaTags.Pipelines.InjectMetaTags
{
	public class MethodTag : IInjectMetaTagsPipelineProcessor
	{

		public string TypeSignature { get; set; }
		public string MethodName { get; set; }
		public string TagName { get; set; }
		public string TagType { get; set; }

		public void Process(InjectMetaTagsPipelineArgs args)
		{
			Profiler.StartOperation(string.Concat("Adding method tag '" + ReflectionUtil.GetMethodName(TypeSignature, MethodName, TagName) + "'"));

			try
			{
				Assert.ArgumentNotNullOrEmpty(TypeSignature, "TypeSignature");
				Assert.ArgumentNotNullOrEmpty(MethodName, "MethodName");
				Assert.ArgumentNotNullOrEmpty(TagName, "TagName");
				Assert.ArgumentNotNullOrEmpty(TagType, "TagType");

				MetaTagType metaType = Settings.GetTypeFromString(TagType);

				args.MetaTags.Add(new Tags.MethodTag(TypeSignature, MethodName, TagName, metaType));
			}
			catch (Exception ex)
			{
				Sitecore.Diagnostics.Tracer.Error("MethodTag failed.", ex);
				Sitecore.Diagnostics.Log.Error("MethodTag failed.", ex, "MethodTag");
			}
			finally
			{
				Profiler.EndOperation();
			}
		}
	
	}
}

