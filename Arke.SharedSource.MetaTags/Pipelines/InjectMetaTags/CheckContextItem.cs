﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using Sitecore.Diagnostics;

namespace Arke.SharedSource.MetaTags.Pipelines.InjectMetaTags
{
	public class CheckContextItem : IInjectMetaTagsPipelineProcessor
	{
		public void Process(InjectMetaTagsPipelineArgs args)
		{
			if (Sitecore.Context.Item == null)
			{
				Tracer.Warning(string.Concat("Not injecting meta tags; no context item"));
				args.AbortPipeline();
			}
		}
	}
}
