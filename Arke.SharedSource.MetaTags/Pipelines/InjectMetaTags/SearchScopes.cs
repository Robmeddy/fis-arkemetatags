﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Diagnostics;
using Arke.SharedSource.MetaTags.Tags;
using Sitecore.Data.Items;

namespace Arke.SharedSource.MetaTags.Pipelines.InjectMetaTags
{
	public class SearchScopes : IInjectMetaTagsPipelineProcessor
	{
		public void Process(InjectMetaTagsPipelineArgs args)
		{
			Sitecore.Diagnostics.Profiler.StartOperation("Adding SearchScopes tag.");
			List<string> scopes = new List<string>();
			try
			{
				scopes.Add(Sitecore.Context.Item.ID.ToString());

				Sitecore.Sites.SiteContext site = Sitecore.Context.Site;
				string startPath = String.Empty;
				if (site != null)
				{
					startPath = site.StartPath;
				}
				foreach (Item item in Sitecore.Context.Item.Axes.GetAncestors())
				{
					if (item.Paths.FullPath.Length >= startPath.Length)
					{
						scopes.Add(item.ID.ToString());
					}
				}
				Tags.StaticTag tag = new Tags.StaticTag(MetaTagType.name, "SearchScopes", Sitecore.StringUtil.Join(scopes.ToArray(), " "));
				args.MetaTags.Add(tag);
			}
			catch (Exception ex)
			{
				Sitecore.Diagnostics.Tracer.Error("SearchScopes MetaTag failed.", ex);
			}
			finally
			{
				Profiler.EndOperation(scopes.Count.ToString() + " scopes added.");
			}
		}
	}
}
