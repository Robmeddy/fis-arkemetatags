﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Diagnostics;
using System.Runtime.Remoting;
using System.Reflection;
using Arke.SharedSource.MetaTags.Tags;

namespace Arke.SharedSource.MetaTags.Pipelines.InjectMetaTags
{
	public class PropertyTag : IInjectMetaTagsPipelineProcessor
	{

		public string TypeSignature { get; set; }
		public string PropertyName { get; set; }
		public string TagName { get; set; }
		public string TagType { get; set; }

		public void Process(InjectMetaTagsPipelineArgs args)
		{
			Profiler.StartOperation(string.Concat("Adding property tag '" + ReflectionUtil.GetPropertyName(TypeSignature, PropertyName, TagName) + "'"));

			try
			{
				Assert.ArgumentNotNullOrEmpty(TypeSignature, "TypeSignature");
				Assert.ArgumentNotNullOrEmpty(PropertyName, "PropertyName");
				Assert.ArgumentNotNullOrEmpty(TagName, "TagName");
				Assert.ArgumentNotNullOrEmpty(TagType, "TagType");

				MetaTagType metaType = Settings.GetTypeFromString(TagType);

				args.MetaTags.Add(new Tags.PropertyTag(TypeSignature, PropertyName, TagName, metaType));
			}
			catch (Exception ex)
			{
				Sitecore.Diagnostics.Tracer.Error("PropertyTag failed.", ex);
				Sitecore.Diagnostics.Log.Error("PropertyTag failed.", ex, "PropertyTag");
			}
			finally
			{
				Profiler.EndOperation();
			}
		}

	}
}

