﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Diagnostics;
using Sitecore.Data.Items;
using Arke.SharedSource.MetaTags.Tags;

namespace Arke.SharedSource.MetaTags.Pipelines.InjectMetaTags
{
	public class GlobalTags : IInjectMetaTagsPipelineProcessor
	{
		public void Process(InjectMetaTagsPipelineArgs args)
		{
			Profiler.StartOperation("Gathering global meta tags");

			try
			{
				Item tagRoot = Sitecore.Context.Database.GetItem(Settings.GlobalTagsFolder);
				if (tagRoot != null)
				{
					foreach (Item tag in tagRoot.Children)
					{
						Profiler.StartOperation(tag.Paths.ContentPath);
						try
						{
							if (tag.TemplateID.Equals(IDs.Templates.StaticMetaTag))
							{
								args.MetaTags.Add(new Tags.StaticTag(tag));
							}
							else if (tag.TemplateID.Equals(IDs.Templates.CustomMetaTag))
							{
								args.MetaTags.Add(ReflectionUtil.GetCustomTag(tag));
							}
							else if (tag.TemplateID.Equals(IDs.Templates.MethodMetaTag))
							{
								args.MetaTags.Add(new Tags.MethodTag(tag));
							}
							else if (tag.TemplateID.Equals(IDs.Templates.PropertyMetaTag))
							{
								args.MetaTags.Add(new Tags.PropertyTag(tag));
							}
							else
							{
								string msg = string.Concat("Ignoring meta tag item: '", tag.Paths.FullPath, "' beause it is not a known tag type.");
								Tracer.Warning(msg);
								Log.Warn(msg, "Arke.SharedSource.MetaTags.Pipelines.InjectMetaTags.GlobalTags");
							}
						}
						catch (Exception ex)
						{
							Sitecore.Diagnostics.Tracer.Error("Global MetaTag '" + tag.Paths.ContentPath + "' failed.", ex);
							Sitecore.Diagnostics.Log.Error("Global MetaTag '" + tag.Paths.ContentPath + "' failed.", ex);
						}
						finally
						{
							Profiler.EndOperation();
						}
					}
				}
			}
			catch (Exception ex)
			{
				Sitecore.Diagnostics.Tracer.Error("GlobalTags failed.", ex);
				Sitecore.Diagnostics.Log.Error("ItemTags failed.", ex, "CustomTag");
			}
			finally
			{
				Profiler.EndOperation();
			}

		}
	}
}