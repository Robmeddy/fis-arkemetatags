﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Diagnostics;
using Sitecore.Data.Items;
using Arke.SharedSource.MetaTags.Tags;
using Sitecore.Data;

namespace Arke.SharedSource.MetaTags.Pipelines.InjectMetaTags
{
	public class ItemTags : IInjectMetaTagsPipelineProcessor
	{
		public void Process(InjectMetaTagsPipelineArgs args)
		{
			if (Sitecore.Context.Item.Fields[IDs.Fields.ItemMetaTagList] == null)
			{
				Tracer.Info("No meta tags defined for this item.");
				return;
			}

			Profiler.StartOperation("Gathering item meta tags");

			try
			{
				Sitecore.Data.Fields.MultilistField fld = Sitecore.Context.Item.Fields[IDs.Fields.ItemMetaTagList];
				foreach (ID id in fld.TargetIDs)
				{
					try
					{
						Item tag = Sitecore.Context.Database.GetItem(id);
						Profiler.StartOperation(tag.Paths.ContentPath);
						if (tag.TemplateID.Equals(IDs.Templates.StaticMetaTag))
						{
							args.MetaTags.Add(new Tags.StaticTag(tag));
						}
						else if (tag.TemplateID.Equals(IDs.Templates.CustomMetaTag))
						{
							args.MetaTags.Add(ReflectionUtil.GetCustomTag(tag));
						}
						else if (tag.TemplateID.Equals(IDs.Templates.MethodMetaTag))
						{
							args.MetaTags.Add(new Tags.MethodTag(tag));
						}
						else if (tag.TemplateID.Equals(IDs.Templates.PropertyMetaTag))
						{
							args.MetaTags.Add(new Tags.PropertyTag(tag));
						}
						else
						{
							string msg = string.Concat("Ignoring meta tag item: '", tag.Paths.FullPath, "' beause it is not a known tag type.");
							Tracer.Warning(msg);
							Log.Warn(msg, "Arke.SharedSource.MetaTags.Pipelines.InjectMetaTags.GlobalTags");
						}

					}
					catch (Exception ex)
					{
						Sitecore.Diagnostics.Tracer.Error("Global MetaTag '" + id + "' failed.", ex);
						Sitecore.Diagnostics.Log.Error("Global MetaTag '" + id + "' failed.", ex);
					}
					finally 
					{
						Profiler.EndOperation();
					}
				}
			}
			catch (Exception ex)
			{
				Sitecore.Diagnostics.Tracer.Error("ItemTags failed.", ex);
				Sitecore.Diagnostics.Log.Error("ItemTags failed.", ex, "CustomTag");

			}
			finally
			{
				Profiler.EndOperation();
			}

		}



	}
}