﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using Sitecore.Diagnostics;

namespace Arke.SharedSource.MetaTags.Pipelines.InjectMetaTags
{


	public class CheckHeader : IInjectMetaTagsPipelineProcessor
	{
		public void Process(InjectMetaTagsPipelineArgs args)
		{
			if (Sitecore.Context.Page.Page.Header == null)
			{
				Tracer.Warning(string.Concat("Not injecting meta tags; no page header"));
				args.AbortPipeline();
			}
		}
	}

}