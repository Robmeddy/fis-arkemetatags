﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Diagnostics;
using System.Runtime.Remoting;
using System.Reflection;
using Arke.SharedSource.MetaTags.Tags;
using Sitecore.Reflection;

namespace Arke.SharedSource.MetaTags.Pipelines.InjectMetaTags
{
	public class CustomTag : IInjectMetaTagsPipelineProcessor
	{
		public string TypeSignature { get; set; }

		public void Process(InjectMetaTagsPipelineArgs args)
		{
			Sitecore.Diagnostics.Profiler.StartOperation("Adding custom tag '" + GetSignatureName() + "'");

			try
			{
				Assert.ArgumentNotNullOrEmpty(TypeSignature, "Class not supplied");
				args.MetaTags.Add(ReflectionUtil.GetCustomTag(TypeSignature));
			}
			catch (Exception ex)
			{
				Sitecore.Diagnostics.Tracer.Error("CustomTag failed.", ex);
				Sitecore.Diagnostics.Log.Error("CustomTag failed.", ex, "CustomTag");
			}
			finally
			{
				Sitecore.Diagnostics.Profiler.EndOperation();
			}
		}

		private string GetSignatureName()
		{
			try
			{
				return new ReflectionUtil.TypeSignature(TypeSignature).Class;
			}
			catch
			{
				return "[unknown]";
			}
		}
	}
}
