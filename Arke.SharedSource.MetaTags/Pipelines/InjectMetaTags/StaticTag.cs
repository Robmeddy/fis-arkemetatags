﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Diagnostics;
using System.Runtime.Remoting;
using System.Reflection;
using Arke.SharedSource.MetaTags.Tags;

namespace Arke.SharedSource.MetaTags.Pipelines.InjectMetaTags
{
	public class StaticTag : IInjectMetaTagsPipelineProcessor
	{

		public string TagName { get; set; }
		public string TagValue { get; set; }
		public string TagType { get; set; }

		public void Process(InjectMetaTagsPipelineArgs args)
		{
            Sitecore.Diagnostics.Log.Info("MNA-ARKE.StaticTag.Process", this);
			Profiler.StartOperation(string.Concat("Adding static tag '" + TagName + "'"));

			try
			{
				Assert.ArgumentNotNullOrEmpty(TagName, "TagName");
				Assert.ArgumentNotNullOrEmpty(TagValue, "TagValue");
				Assert.ArgumentNotNullOrEmpty(TagType, "TagType");

				MetaTagType metaType = Settings.GetTypeFromString(TagType);

				args.MetaTags.Add(new Tags.StaticTag(Settings.GetTypeFromString(TagType), TagName, TagValue));
			}
			catch (Exception ex)
			{
				Sitecore.Diagnostics.Tracer.Error("StaticTag failed.", ex);
				Sitecore.Diagnostics.Log.Error("StaticTag failed.", ex, "StaticTag");
			}
			finally
			{
				Profiler.EndOperation();
			}
		}

	}
}

